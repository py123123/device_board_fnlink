// #include "hdf_log.h"
#include "uart_if.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "hdf_log.h"

DevHandle UartSetup(void)
{
    int32_t ret;
    uint32_t port;  
    DevHandle handle = NULL;
    uint8_t wbuff[5] = { 1, 2, 3, 4, 5 };
    uint8_t rbuff[5] = { 0 };
    struct UartAttribute attribute;
    attribute.dataBits = UART_ATTR_DATABIT_8;   /* UART传输数据位宽，一次传输7个bit */
    attribute.parity = UART_ATTR_PARITY_NONE;   /* UART传输数据无校检 */
    attribute.stopBits = UART_ATTR_STOPBIT_1;   /* UART传输数据停止位为1位 */
    attribute.rts = UART_ATTR_RTS_DIS;          /* UART禁用RTS */
    attribute.cts = UART_ATTR_CTS_DIS;          /* UART禁用CTS */
    attribute.fifoRxEn = UART_ATTR_RX_FIFO_DIS;  /* UART使能RX FIFO */
    attribute.fifoTxEn = UART_ATTR_TX_FIFO_DIS;  /* UART使能TX FIFO */
    /* UART设备端口号，要填写实际平台上的端口号 */
    port = 2; 
    /* 获取UART设备句柄 */
    HDF_LOGE("open uart 2");
    handle = UartOpen(port);
    if (handle == NULL) {
        HDF_LOGE("UartOpen: failed!\r\n");
        return -2;
    }
    /* 设置UART波特率为9600 */
    ret = UartSetBaud(handle, 115200);
    if (ret != 0) {
        HDF_LOGE("UartSetBaud: failed, ret %d\r\n", ret);
        goto _ERR;
    }
    /* 设置UART设备属性 */
    ret = UartSetAttribute(handle, &attribute);
    if (ret != 0) {
        HDF_LOGE("UartSetAttribute: failed, ret %d\r\n", ret);
        goto _ERR;
    }
    /* 设置UART传输模式为非阻塞模式 */
    ret = UartSetTransMode(handle, UART_MODE_RD_BLOCK);
    if (ret != 0) {
        HDF_LOGE("UartSetTransMode: failed, ret %d\r\n", ret);
        goto _ERR;
    }

    return handle;
 
_ERR:
    /* 销毁UART设备句柄 */
    UartClose(handle); 
    return -1;
}

void UartTestSampleTask(void)
{
    uint8_t rbuff;
    uint8_t * pbuff = &rbuff;
    int ret;
    
    HDF_LOGE("11111111%s:%d\r\n",__func__,__LINE__);

    DevHandle handle = UartSetup();
    if( handle < 0 ){
        HDF_LOGE("open_port error\r\n");
        return 1;
    }
    ret = UartWrite(handle, ">", 1);
    if (ret < 0) {
        HDF_LOGE("UartRead: failed, ret %d\r\n", ret);
        goto _ERR;
    }

    while(1){
        ret = UartRead(handle, pbuff, 1);
        if (ret < 0) {
            HDF_LOGE("UartRead: failed, ret %d\r\n", ret);
            goto _ERR;
        }
        ret = UartWrite(handle, pbuff, 1);
        if (ret < 0) {
            HDF_LOGE("UartRead: failed, ret %d\r\n", ret);
            goto _ERR;
        }
        if(*pbuff == '\n'){
            ret = UartWrite(handle, ">", 1);
            if (ret < 0) {
                HDF_LOGE("UartRead: failed, ret %d\r\n", ret);
                goto _ERR;
            }
            printf("\r\n");        
        } else {
            printf("%c",*pbuff);
        }
    }


_ERR:
    /* 销毁UART设备句柄 */
    UartClose(handle); 
}

void UartTestSample(void)
{
    osThreadAttr_t attr = {0};
    printf("UartTestSample Entry\r\n");
    HDF_LOGE("UartTestSample Entry\r\n");
    attr.stack_size = 0x1000; 
    attr.priority = osPriorityNormal;
    attr.name = "UartTestSample";
    if (osThreadNew((osThreadFunc_t)UartTestSampleTask, NULL, &attr) == NULL) {
        HDF_LOGE("Failed to create UartTestSampleTask\r\n");
    }
}

APP_FEATURE_INIT(UartTestSample);
